#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import requests
from bs4 import BeautifulSoup
import itertools
import re

def prenoms():
    page = requests.get('http://fr.wikipedia.org/wiki/Liste_de_pr%C3%A9noms_fran%C3%A7ais_et_de_la_francophonie').text
    soup = BeautifulSoup(page)
    names = soup.find_all('dd')
    garcons = []
    filles = []
    for elt in names:
        try:
            genre = elt.find('span').get_text()
            prenom = elt.find('a').get_text(strip=True)
        except:
            continue
        if genre in ['(m)', '(x)']:
            garcons.append(prenom)
        if genre in ['(f)', '(x)']:
            filles.append(prenom)
    return garcons, filles

def pays():
    page = requests.get('http://www.eguens.com/v2/pays/liste-pays-du-monde-entier.php').text
    soup = BeautifulSoup(page)
    t = soup.find_all('table')[2]
    pays = []
    for elt in t.find_all('tr'):
        try:
            pays.append(elt.find('td').get_text().replace('\\', ''))
        except:
            continue
    return pays


def metiers():
    page = requests.get('http://www.cio-pau.org/rMET_MEA.php').text
    soup = BeautifulSoup(page)
    metiers = []
    t = soup.find_all('ul')
    for metier in itertools.chain(*[e.find_all('li') for e in t]):
        content = metier.get_text(strip=True)
        content = re.sub('\(.+\)', '', content)
        metiers.append(content)
    return metiers

def fruits_legumes():
    page = requests.get('http://popoblog.unblog.fr/files/2009/12/fruitsetlgumes.txt').text
    liste = list(map(str.strip, page.split('\n')))
    liste2 = liste[:]
    return list(filter(lambda x: x[:-1] not in liste2, liste))

def animaux():
    vert = BeautifulSoup(requests.get('http://educ.csmv.qc.ca/mgrparent/vieanimale/vert.htm').text)
    invert = BeautifulSoup(requests.get('http://educ.csmv.qc.ca/mgrparent/vieanimale/invert.htm').text)
    liste = vert.find_all('a') + invert.find_all('a')
    liste = [e.get_text(strip=True) for e in liste]
    liste = list(filter(lambda x: 'natomie' not in x and x, liste))
    liste = [' '.join(e.split()) for e in liste]
    liste = list(set([re.sub('\(.+\)', '', e) for e in liste]))
    liste = [e.replace(',', '') for e in liste]
    return liste

def celebrites():
    soup = BeautifulSoup(requests.get('http://www.keskeces.com/celebrite/').text)
    return [e.get_text(strip=True) for e in soup.find('div', 'table').find_all('a')]
